<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $primaryKey = 'idnoticia';

    protected $fillable = [
        'idcatnoticia' ,
        'titulo' ,
        'contenido', 
        'imagen' ,
    ];  


    public function getCategoryName(){
        return Categoria::where('idcatnoticia' , $this->idcatnoticia)->first()->titulo;
    }

    public function getCategoryId(){
        return Categoria::where('idcatnoticia' , $this->idcatnoticia)->first()->idcatnoticia;
    }
}
