<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ocurrencias extends Model
{
    protected $table = "ocurrencias_n";
    protected $primaryKey = "idocurrencia";
}
