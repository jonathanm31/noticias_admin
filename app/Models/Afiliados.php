<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Afiliados extends Model
{
    protected $table = "afiliados";
    protected $primaryKey = "idafiliado";
    protected $timestamp = "false";
}
