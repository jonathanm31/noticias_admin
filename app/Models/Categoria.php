<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $primaryKey = 'idcatnoticia';

    protected $fillable = [
        'idparent' , 'titulo' ,     
    ];

    public function getCategoryNamec() {
        if(  $this->idparent !== null){
            return Categoria::where('idcatnoticia' , $this->idparent)->first()->titulo;
        } else{
            return " ";
        }
        
    }
    
    public function getCategoryIdc() {
       if($this->idparent !== null ) {
        return Categoria::where('idcatnoticia' , $this->idparent)->first()->idcatnoticia;
       } else{
           return "Escoger ... ";
       }
        
    }
   
}
