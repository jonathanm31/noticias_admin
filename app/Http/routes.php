<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('listNoticias', 'noticias\NoticiasController@index');
Route::get('listCategorias', 'noticias\CatNoticiasController@index')->name('list_categoria');
Route::get('newCategoria' , 'noticias\CatNoticiasController@insert_categoria');
Route::post('newCategoria' , 'noticias\CatNoticiasController@store')->name('nueva_categoria');

Route::get('listNoticia' , 'noticias\NoticiasController@index')->name('list_noticia');
Route::get('newNoticia' , 'noticias\NoticiasController@insert_noticia')->name('nueva_noticia');
Route::post('newNoticia' , 'noticias\NoticiasController@store')->name('nueva_noticia');
Route::get('detalleNoticia' , 'noticias\NoticiasController@detalle_noticia')->name('detalle_noticia');


Route::get('Buscar', 'noticias\NoticiasController@Buscar')->name('buscar_noticias');
Route::get('BuscarPorCategoria', 'noticias\NoticiasController@BuscarPorCategoria')->name('buscar_por_categoria');
Route::get('editar/{id}' , 'noticias\NoticiasController@edit')->name('edit_noticia');
Route::post('editarNoticia' , 'noticias\NoticiasController@update')->name('edit_noticial');


Route::get('editarc/{id}' , 'noticias\CatNoticiasController@edit')->name('edit_categoria');
Route::post('editarCategoria' , 'noticias\CatNoticiasController@update')->name('edit_categorial');

Route::delete('eliminar', 'noticias\NoticiasController@destroy' )->name('delete_noticia');
Route::delete('eliminarc', 'noticias\CatNoticiasController@destroy' )->name('delete_categoria');





Route::get('denied',function(){
   return view('auth.permisos');
});
$this->get('logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'guest_group'],function(){
    $this->get('login', 'Auth\AuthController@showLoginForm');
    $this->post('login', 'Auth\AuthController@login');
    $this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    $this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    $this->post('password/reset', 'Auth\PasswordController@reset');
});


Route::group(['middleware' => 'usuario_group'],function(){


});


Route::group(['middleware' => 'admin_group'],function(){
  
});
