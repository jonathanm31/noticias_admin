<?php

namespace App\Http\Controllers\noticias;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Categoria;

class CatNoticiasController extends Controller
{
    public function index(){
        $categorias = Categoria::get();
        $noticias = Noticia::get();

        return view('noticias.categorias')->with('categorias',$categorias)->with('noticias', $noticias);
    }

    public function insert_categoria(){
        $categorias = Categoria::get();
        return view('noticias.nueva_categoria')->with('categorias',$categorias);
    }
    
    public function store(Request $request){
        
        $this->validate($request, [
            'titulo' =>'required'
        ]); 

        $categoria = new Categoria();
        $categoria->idparent = $request->idparent;
        $categoria->titulo = $request->titulo;
        $categoria->state = $request->state;

        $categoria->save();
             
        return $this->index();
    }

    //like the index
    public function cat_editar(){
        $categorias = Categoria::get();
        
        return view('noticias.modificar_categoria')->with('categorias' , $categorias);
    }

    public function edit($idcatnoticia){
        $categoria = Categoria::find($idcatnoticia);
        
        return $this->cat_editar()->with('categoria' , $categoria);
        
    }

    public function update(Request $request){

        $categoria = Categoria::find($request->idcatnoticia);

        $categoria->idparent = $request->idparent;
        $categoria->titulo = $request->titulo;
        $categoria->state = $request->state;

        $categoria->save();

        return $this->index();
    }

    public function destroy(Request $request){

        
        $categoria = Categoria::find($request->idcatnoticia);

        $categoria->delete();
        return $this->index();
    }
}
