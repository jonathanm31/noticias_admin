<?php

namespace App\Http\Controllers\noticias;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Noticia;
use App\Models\Categoria;

class NoticiasController extends Controller
{
    public function index()
    {
        $categoriaSeleccionada = null;
        $noticias = Noticia::get();
        $categorias = Categoria::get();
        $noticias = Noticia::paginate(10);
        return view('noticias.noticias')->with('noticias', $noticias)->with('categorias',$categorias)->with('categoriaSeleccionada',$categoriaSeleccionada);
    }
    public function Buscar(Request $request)
    {
        $categoriaSeleccionada = null;
        $categorias = Categoria::get();
        $query = $request['query'];
        $noticias= Noticia::where('titulo', 'like',"%".$query."%")->paginate(10);
                    
        return view('noticias.noticias')->with('noticias', $noticias)->with('categorias',$categorias)->with('categoriaSeleccionada',$categoriaSeleccionada);
    }
    public function BuscarPorCategoria(Request $request)
    {
        $categorias = Categoria::get();
        $query = $request['query'];
        $categoriaSeleccionada = null;
        if($query){
            $noticias= Noticia::where('idcatnoticia', '=',$query)->paginate(10);
            $categoriaSeleccionada = Categoria::find($query);
            
        }else{
         $noticias = Noticia::paginate(10);
        
        }
        return view('noticias.noticias')->with('noticias', $noticias)->with('categorias',$categorias)->with('categoriaSeleccionada',$categoriaSeleccionada);
                    
         
    }
    public function insert_noticia()
    {
        $categorias = Categoria::get();

        return view('noticias.nueva_noticia')->with('categorias', $categorias);
    }

    public function detalle_noticia(Request $request)
    {
        $idnoticia = $request['idnoticia'];
        $noticia = Noticia::find($idnoticia);

        $categoria = Categoria::find($noticia->idcatnoticia);
        $nombreCategoria = $categoria->titulo;

        $nombreCategoriaPadre="";
        $nombreCategoriaAbuelo="";
        if ($categoria->idparent != null) {
            $categoriaPadre = Categoria::find($categoria->getCategoryIdc());
            $nombreCategoriaPadre = $categoriaPadre->titulo;
            $nombreCategoria= " > ".$nombreCategoria;
            if($categoriaPadre->idparent!=null){
                $nombreCategoriaAbuelo = $categoriaPadre->getCategoryNamec();
                $nombreCategoriaPadre=" > ".$nombreCategoriaPadre;
            }
        }
        
      





        return json_encode(["noticia" => $noticia, "nombreCategoriaAbuelo" => $nombreCategoriaAbuelo, "nombreCategoriaPadre" => $nombreCategoriaPadre, "nombreCategoria" => $nombreCategoria]);
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'titulo' => 'required',
            'contenido' => 'required',
            'imagen' => 'required'
        ]);


        $noticia = new Noticia();



        $noticia->idcatnoticia = $request->idcatnoticia;
        $noticia->titulo = $request->titulo;
        $noticia->contenido = $request->contenido;




        $noticia->save();

        if ($request->hasFile('imagen')) {
            $file = $request->file('imagen');
            $name = time() . $file->getClientOriginalName();
            $file->move(public_path() . '/imagesNot/' . $noticia->idnoticia . '/', $name);
        }

        $noticia->imagen = $name;
        $noticia->save();
        return redirect('listNoticia');


    }

    public function cat_editar()
    {
        $categorias = Categoria::get();

        return view('noticias.modificar_noticia')->with('categorias', $categorias);
    }

    public function edit($idnoticia)
    {
        $noticia = Noticia::find($idnoticia);
       //$categorias = Categoria::get();
        return $this->cat_editar()->with('noticia', $noticia);

    }

    public function update(Request $request)
    {

        $noticia = Noticia::find($request->idnoticia);

        if ($request->hasFile('imagen')) {
            $file = $request->file('imagen');
            $name = time() . $file->getClientOriginalName();
            $noticia->imagen = $name;
            $file->move(public_path() . '/imagesNot/' . $noticia->idnoticia . '/', $name);

        }
   
        $noticia->idcatnoticia = $request->idcatnoticia;
        $noticia->titulo = $request->titulo;
        $noticia->contenido = $request->contenido;
        $noticia->state = $request->state;

        $noticia->save();

        /* redirect(base_url() . 'NoticiasController/index'); */
        return redirect('listNoticia');



    }

    public function destroy(Request $request)
    {
        $noticia = Noticia::find($request->idnoticia);
        $noticia->delete();


    }
}