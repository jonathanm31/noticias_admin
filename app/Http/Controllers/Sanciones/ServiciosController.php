<?php

namespace App\Http\Controllers\Sanciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Administrador;
use Illuminate\Support\Facades\Auth;
use App\Models\Faltas;

class ServiciosController extends Controller
{
    function __construct()
    {
    }
    function generateRandomString($length = 30) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_=-+';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    function auntenticacion(Request $request){
        $email = $request['email'];
        $password = $request['password'];
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $usuario = Administrador::select('idadministrador', 'nombres','apellidos', 'token')
                                     ->where('email','=',$email)->first();
            $token = $this->generateRandomString();
            $usuario->token = $token ;
            $usuario->save();
            return json_encode(['status' => 200, 'data' => $usuario]);
        }else{
            return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);
        }


    }
    function afiliado_info(Request $request){
        $nombre = $request['query'];
        $afiliados = DB::table('afiliadoinfo_n')
            ->where('nombres', 'like', '%'.$nombre.'%')
            ->leftJoin('puestos_n','afiliadoinfo_n.idpuesto','=','puestos_n.idpuesto')->get();
        if($afiliados) return json_encode(['status' => 200, 'data' => $afiliados]);
        return json_encode(['status'=> 100, 'mensaje' => 'no hay afiliados con ese nombre', 'data' => []]);

    }


}
