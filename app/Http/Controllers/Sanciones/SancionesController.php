<?php

namespace App\Http\Controllers\Sanciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\Sanciones;
use App\Http\Controllers\Controller;

class SancionesController extends Controller
{

    public function index(Request $request){
        return view('Sanciones.sanciones');
    }
    public function get_all(Request $request){
        $sanciones = Sanciones::get();
        return json_encode(array('status'=> 200, 'data' => $sanciones));
    }
    public function insertar(Request $request){
        $nombre = $request['nombre'];
        $sancion = new Sanciones;
        $sancion->sancion = $nombre;
        $sancion->state = $request['estado'];
        $sancion->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se Guardo Correctamente'));
    }
    public function modificar(Request $request){
        $nombre = $request['nombre'];
        $state = $request['estado'];
        $sancion = Sanciones::find($request['idsancion']);
        $sancion->sacion = $nombre;
        $sancion->state = $state;
        $sancion->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se modifico Correctamente'));

    }
    public function eliminar(Request $request){
        $sancion = Sanciones::find($request['idsancion']);
        $sancion->delete();
        return json_encode(array('state'=>200,'mensaje'=> 'Se elimino Correctamente'));
    }
}
