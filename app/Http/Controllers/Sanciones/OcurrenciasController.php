<?php

namespace App\Http\Controllers\Sanciones;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\Faltas;
use App\Models\Administrador;
use App\Models\Ocurrencias;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class OcurrenciasController extends Controller
{

    public function index(Request $request){
        $faltas = DB::table('faltas_n')->get();
        $sanciones  = DB::table('sanciones_n')->get();
        $afiliados = DB::table('afiliados')->get();
        $califiaciones = DB::table('calificaciones_n')->get();
        return view('Sanciones.ocurrencias')->with( 'data' , array('faltas' => $faltas, 'sanciones' => $sanciones, 'afiliados' => $afiliados, 'calificaciones'=> $califiaciones));
    }
    public function get_all(Request $request){
        $ocurrencias = DB::table('ocurrencias_n')->join('afiliados','ocurrencias_n.idafiliado','=','afiliados.idafiliado')->get();
        return json_encode(array('status'=> 200, 'data' => array('ocurrencias' => $ocurrencias)));
    }
    public function validar_login(Request $request){
        $token = $request['token'];
        $idadmin = $request['idadministrador'];
        $usuario = Administrador::where('token','=',$token)->where('idadministrador','=',$idadmin)->first();
        if($usuario){
            return true;
        }else{
            return false;
        }

    }
    public function validar_admin(Request $request){
        $usuario = Administrador::where('token','=',$token)->where('idadministrador','=',$idadmin)->first();

    }

    public function get_all_filtro(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);

        $desde = $request['desde'];
        $hasta = $request['hasta'];
        $idocurrencia = $request['idocurrencia'];
        $dni = $request['dni'];
        $tam_pag = $request['tam_pag'];
        if(!$tam_pag) $tam_pag = 15;
        //$page = $request["page"];

        if($desde == "-1")$desde = "";
        if($hasta == "-1")$hasta = "";
        if($idocurrencia == "-1")$idocurrencia = "";
        if($dni == "-1")$dni = "";
        $ocurrencias = DB::table('ocurrencias_n')
            ->select('ocurrencias_n.*','afiliadoinfo_n.*','sanciones_n.sancion','sanciones_n.idsancion','faltas_n.idfalta','faltas_n.falta')
            ->whereBetween('ocurrencias_n.fec_ocurrencia', array( $request['desde'],  $request['hasta']))
            ->orwhere('afiliadoinfo_n.idafiliado','=',$request['dni'])
            ->orwhere('ocurrencias_n.idocurrencia','=',$request['idocurrencia'])
            ->leftJoin('faltas_n','ocurrencias_n.idfalta','=','faltas_n.idfalta')
            ->leftJoin('sanciones_n','ocurrencias_n.idsancion','=','sanciones_n.idsancion')
            ->leftJoin('afiliadoinfo_n','ocurrencias_n.idafiliado','=','afiliadoinfo_n.idafiliado')->simplePaginate($tam_pag);

        foreach ($ocurrencias as $index => $ocurrencia){
            $documentos = json_decode($ocurrencia->documentos);
            if($documentos){
                foreach ($documentos as $index => $documento){
                    $documentos[$index] = "https://app.mapsalud.com/clientes/somosoh/admin/public/ocurrencias/".$ocurrencia->idocurrencia.$documento;
                }
                $ocurrencia->documentos = $documentos;
            }

            $calificacion = json_decode($ocurrencia->calificacion);
            foreach ($calificacion as $index => $c){
                $calificacion[$index] = DB::table('calificaciones_n')->where('idcalificacion','=', $c)->first();
            }
            $ocurrencia->calificacion = $calificacion;
        }
        return json_encode(array('status'=> 200, 'data' => array('ocurrencias' => $ocurrencias)));
    }
    public function upload_file(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);

        $idocurrencia = $request['idocurrencia'];
        $file = $request->file('archivo');
        if ( !$request->hasFile('archivo') ||  !$file->isValid()) {
            return json_encode(array('status'=> 100, 'mensaje' => "Archivo invalido."));
        }

        if($file->getSize() > 200000000 ){
            return json_encode(array('status'=> 100, 'mensaje' => "El archivo es de ".$file->getSize() ." supera los 20 MB "));
        }
        switch ($file->extension()){
            case "jpeg":
                break;
            case "png":
                break;
            case "JPG":
                break;
            case "PNG":
                break;
            case "doc":
                break;
            case "docx":
                break;
            case "pdf":
                break;
            case "JPEG":
                break;
            case "DOC":
                break;
            default:
                return json_encode(array('status'=> 100, 'mensaje' => "El formato ".$file->extension() ." no es valido, los formatos son: png, jpg, jpeg, doc, docx, pdf "));
                break;
        }

        $ocurrencia = Ocurrencias::find($request['idocurrencia']);
        if($ocurrencia == null){
            return json_encode(array('status'=> 100, 'mensaje' => "No existe esa ocurrencia."));
        }
        if($ocurrencia->documentos != null){
            $documentos = json_decode($ocurrencia->documentos );
        }else{
            $documentos = [] ;
        }
        $time = time();
        array_push($documentos,$time.'_'.$file->getClientOriginalName());
        $file->move('ocurrencias/'.$idocurrencia, $time.'_'.$file->getClientOriginalName());
        $ocurrencia->documentos = json_encode($documentos);
        $ocurrencia->save();

        return json_encode(array('status'=> 200, 'mensaje' => "Archivo Guardado Exitosamente."));
    }
    public function insertar(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);

        $ocurrencia = new Ocurrencias;
        if(!$request['falta'] || !$request['sancion']  || !$request['fecha']){
            return json_encode(array('state'=>100,'mensaje'=> 'Campos incompletos'));
        }
        $ocurrencia->idafiliado = $request['afiliado'];
        $ocurrencia->idfalta = $request['falta'];
        $ocurrencia->idsancion = $request['sancion'];
        $ocurrencia->descripcion = $request['descripcion'];
        $ocurrencia->calificacion = $request['calificacion'];
        $ocurrencia->fec_ocurrencia = $request['fecha'];
        $ocurrencia->fec_suspencion_ini = $request['inicio'];
        $ocurrencia->fec_suspencion_fin = $request['fin'];
        $ocurrencia->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se Guardo Correctamente','data' => ['idocurrencia' => $ocurrencia->idocurrencia]));
    }
    public function modificar(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);

        $ocurrencia = Ocurrencias::find($request['idocurrencia']);
        if(!$request['idocurrencia']){
            return json_encode(array('state'=>100,'mensaje'=> 'Campos incompletos'));
        }
        if( $request['afiliado']){
            return json_encode(array('state'=>100,'mensaje'=> 'Si desea cambiar de afiliado, debe eliminar la ocurrencia actual y crear una nueva con el afiliado correspondiente'));
        }
        if($request['falta'])$ocurrencia->idfalta = $request['falta'];
        if($request['sancion'])$ocurrencia->idsancion = $request['sancion'];
        if($request['descripcion'])$ocurrencia->descripcion = $request['descripcion'];
        if($request['calificacion']) $ocurrencia->calificacion = $request['calificacion'];
        if($request['fecha'])$ocurrencia->fec_ocurrencia = $request['fecha'];
        if($request['inicio'])$ocurrencia->fec_suspencion_ini = $request['inicio'];
        if($request['fin'])$ocurrencia->fec_suspencion_fin = $request['fin'];
        $ocurrencia->save();
        return json_encode(array('state'=>200,'mensaje'=> 'Se modifico Correctamente'));

    }
    public function eliminar(Request $request){
        if(!$this->validar_login($request)) return json_encode(['status' => 100, 'mensaje' => 'No tiene acceso']);

        $ocurrencia = Ocurrencias::find($request['idocurrencia']);
        $path = 'ocurrencias/'.$request['idocurrencia'];
        if(is_dir($path)) {
            array_map('unlink', glob($path."/*.*"));
            rmdir($path);
        }
        $ocurrencia->delete();
        return json_encode(array('state'=>200,'mensaje'=> 'Se elimino Correctamente'));

    }




}
