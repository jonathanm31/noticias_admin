<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'uploadfile_ocurrencias','ocurrencias_filtro','insertar_ocurrencia','modificar_ocurrencia','eliminar_ocurrencia','authentication','get_afiliados','get_faltas','get_sanciones','get_calificaciones'
    ];
}
