<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categorias::create(['titulo' => 'Tareas']);
        Categorias::create(['titulo' => 'Recordatorios']);
        Categorias::create(['titulo' => 'Recursos']);
        Categorias::create(['titulo' => 'Eventos']);

    }
}
