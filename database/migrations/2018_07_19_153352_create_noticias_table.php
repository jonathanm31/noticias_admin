<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->increments('idnoticia');
            $table->integer('idcatnoticia')->unsigned();
            $table->foreign('idcatnoticia')->references('idcatnoticia')->on('categorias');
            $table->text('titulo')->nullable();
            $table->longText('contenido')->nullable();
            $table->text('imagen')->nullable();
            $table->integer('state')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('noticias');
    }
}
