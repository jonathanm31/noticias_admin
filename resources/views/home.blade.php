@extends('layouts.app')

@section('content')
    <script type="application/javascript" src="javascript/Chart.js">    </script>
<div class="container">
    <div class="row">
          <div class="col-md-6 col-xd-12">
              <div class="panel panel-flat">
                  <div class="panel-heading">
                      <h6 class="panel-title text-semibold">Mensajes Aprobados y Denegados<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                  </div>
                  <div class="panel-body">
                      <div class="chart-container">
                          <canvas id="statsbystate" width="100%" height="100"></canvas>
                      </div>
                  </div>
              </div>
          </div>
        <div class="col-md-6 col-xd-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Cantidad de Mensajes por Cupon<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                </div>
                <div class="panel-body">
                    <div class="chart-container">
                        <canvas id="statsbycupon" width="100%" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        function getRandomRgb() {
            var num = Math.round(0xffffff * Math.random());
            var r = num >> 16;
            var g = num >> 8 & 255;
            var b = num & 255;
            return 'rgb(' + r + ', ' + g + ', ' + b + ')';
        }
        var currentLocation =  $('meta[name="base_url"]').attr('content')+'/';

        var data_stats;
        $.get(currentLocation+'stats',function(data){
            data_stats = JSON.parse(data);
            console.log(data_stats);
            if(data_stats.status === '200'){
                var data_bar = [];
                var label_bar = [];
                var color_b = [];
                var data_pie = [];
                var label_pie = [];
                var color_p=[];
                $.each(data_stats.data.statsbystate ,function(index, value){
                    data_bar.push( parseInt(value.cantidad_messages));
                    label_bar.push(value.state);
                    color_b.push(getRandomRgb());
                });
                $.each(data_stats.data.statsbycupon ,function(index, value){
                    data_pie.push( parseInt(value.cantidad_messages));
                    label_pie.push('Cupon '+value.idcupon);
                    color_p.push(getRandomRgb());
                });

                var ctx_S = document.getElementById("statsbystate").getContext('2d');
                var ctx_C = document.getElementById("statsbycupon").getContext('2d');
                var barChart = new Chart(ctx_S, {
                    type: 'bar',
                    data: {
                        labels: ['Pendiente','Aprobados','Denegados','Cancelados'],
                        datasets: [{
                            label: '# Mensajes',
                            data: data_bar,
                            backgroundColor: [
                                '#889B15',
                                '#5B5C61'

                            ],
                            borderColor: [
                                '#889B15',
                                '#5B5C61'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
                // For a pie chart
                var PieChart = new Chart(ctx_C,{
                    type: 'bar',
                    data: {
                        labels: label_pie,
                        datasets: [{
                            label: '# de mensajes',
                            data: data_pie,
                            backgroundColor: color_p,
                            borderColor: color_p,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }

        });


    </script>
@endsection
