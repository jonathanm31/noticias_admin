@extends('layouts.app')

@section('content')

<h1> Nueva Categoria </h1>



<form role="form" method="POST" action="{{ route('nueva_categoria') }}" enctype="multipart/form-data">

    {{ csrf_field() }}
    <div class="form-group">
      <label for="inputState">Categoria a la que pertenece</label>
      <select id="inputState" class="form-control" name='idparent'>
        <option value="0" selected disabled>Escoger...</option>
        @forelse ($categorias as $categoria)
          <option value="{{ $categoria->idcatnoticia }}"> {{ $categoria->titulo }}</option>
        @empty

        @endforelse
        
      </select>
    </div>
    
    <div class="form-group">
      <label for="titulo">Titulo</label>
      <input type="text" class="form-control" name="titulo" placeholder="Another input">
      @if($errors->has('titulo'))
      <span style ="color:red;"> El titulo es requerido </span>
    @endif
    </div>
    <button type="submit" class="btn btn-primary">Registrar</button>
  </form>
@endsection