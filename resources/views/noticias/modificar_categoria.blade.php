@extends('layouts.app')

@section('content')

<h1> Nueva Categoria </h1>
    

<form role="form" method="POST" action="{{ route('edit_categorial') }}" enctype="multipart/form-data">

  
    {{ csrf_field() }}
    <h1></h1>
    <div class="form-group">
      <label for="inputState">Categoria a la que pertenece</label>
      <select id="inputState" class="form-control" name='idparent'>
      <option value="{{ $categoria->getCategoryIdc() }} " selected> {{ $categoria->getCategoryNamec() }}</option>
        @forelse ($categorias as $__categoria)
          <option value="{{ $__categoria->idcatnoticia }}"> {{ $__categoria->titulo }}</option>
        @empty

        @endforelse
      </select>
    </div>
    <input type="hidden"  name="idcatnoticia" value="{{ $categoria->idcatnoticia}}">
    <div class="form-group">
      <label for="titulo">Titulo</label>
    <input type="text" class="form-control" name="titulo" placeholder="titutlo" value="{{ $categoria->titulo }}"></input>
    </div>
    <button type="submit" class="btn btn-primary">Registrar</button>
  </form>
@endsection