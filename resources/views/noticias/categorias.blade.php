@extends('layouts.app')

@section('content')

<h1> Categorias</h1>


<table class="table">
    <thead class="thead-dark">

     
      <tr>
        <th scope="col">ID</th>
        <th scope="col">TITULO</th>
        <th scope="col">CATEGORIA PADRE</th>
        <th scope="col">EDITAR</th>
        <th scope="col">ELIMINAR</th>
      </tr>
    </thead>
    <tbody>
      @foreach($categorias as $categoria)
      <tr>
        <td> {{ $categoria->idcatnoticia }} </td>
        <td> {{ $categoria->titulo }} </td>
      <td>  {{ $categoria->getCategoryNamec()}}</td>
        <td>
        <a class="btn btn-primary btn-sm " href="editarc/{{ $categoria->idcatnoticia }}"> Editar</a>
        </td>
        <td>
          <form action="eliminarc" method="POST" >
            {{ method_field('delete') }}
            {{ csrf_field() }}

            
            <input type="hidden"  name="idcatnoticia" value="{{ $categoria->idcatnoticia}}">
            <button class="btn btn-danger" type="submit">Delete</button>
        </form>
        </td>
      </tr>
      
      @endforeach
    </tbody>
  </table>
  
  
@endsection
