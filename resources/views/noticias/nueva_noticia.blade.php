@extends('layouts.app')


<head>
<link href="{{ asset('/css/admin-styles/referencias.css') }}" rel="stylesheet">
</head>

@section('breadcrumb')
<label id="labelRectangle">Noticias</label>
@endsection @section('content')


<form role="form" method="POST" action="{{ route('nueva_noticia') }}" enctype="multipart/form-data">
<div class="row">
        <div class="col-md-5 alineado">
        <label id="titulo1"> Nueva Noticia </label>
        </div>
        
        <div class="col-md-7">
            <div class="box-right">
                <button type="submit" style="margin-top:15px"class="buttonNew-Edit">Registrar</a>
            </div>
        </div>
    </div>



    {{ csrf_field() }}
    
        
    
    <div class="form-group">
    <output id="list">
    <img class="thumb" width="320" src="http://localhost:8000/images/imagenVacia.png">
    </output>
              <input class="inputFile" type="file" id="files" name="imagen"  placeholder="Another input">
              <div id="divCambiarImagen" class="buttonCambiarImagen">CARGAR IMAGEN</div>
        
        @if($errors->has('imagen'))
          <span style ="color:red;"> La imagen es requerida </span>
        @endif
    </div>

    <div class="form-group">
            
            <label for="inputState" class="label-Roboto">Categoría</label>
            <select style="background-color: #ffffff !important" id="inputState" class="form-control combobox" name='idcatnoticia'>
            
              @forelse ($categorias as $categoria)
                <option value="{{ $categoria->idcatnoticia }}"> {{ $categoria->titulo }}</option>
              @empty
      
              @endforelse
            </select>
          </div>
          
    <div class="form-group" >
      <label for="titulo" class="label-Roboto">Titulo</label>
      <input type="text" style= "max-width:550px"class="form-control textBox" name="titulo" placeholder="Another input">
        @if($errors->has('titulo'))
          <span style ="color:red;"> El titulo es requerido </span>
        @endif
    </div>
    <div class="form-group">
        <label for="contenidol" class="label-Roboto">Contenido</label>
        <textarea id="summary-ckeditor" placeholder="Balabala"  name="contenido"></textarea>
        @if($errors->has('contenido'))
          <span style ="color:red;"> El contenido es requerido </span>
        @endif
      </div>
      
   
  </form>


  <script>
  
    $(".inputFile").change(function(){
      $('#divCambiarImagen').html("CAMBIAR IMAGEN");
});
    function archivo(evt) {
        var files = evt.target.files; // FileList object
         
          //Obtenemos la imagen del campo "file". 
        for (var i = 0, f; f = files[i]; i++) {         
             //Solo admitimos imágenes.
             if (!f.type.match('image.*')) {
                  continue;
             }
         
             var reader = new FileReader();
             
             reader.onload = (function(theFile) {
                 return function(e) {
                 // Creamos la imagen.
                        document.getElementById("list").innerHTML = ['<img class="thumb" width="320" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                 };
             })(f);
   
             reader.readAsDataURL(f);
         }
  }
               
        document.getElementById('files').addEventListener('change', archivo, false);
  </script>
  <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>

<script>

    CKEDITOR.replace( 'summary-ckeditor',
{

width: '550px',
height: '189px',
removePlugins: 'elementspath',

removeButtons:'Paste,About,PasteText,PasteFromWord,Undo,Redo,Link,Unlink,Subscript,Superscript',


} );
    

</script>

@endsection



