@extends('layouts.app')

<head>
<link href="{{ asset('/css/admin-styles/referencias.css') }}" rel="stylesheet">


 
</head>
 
 @section('breadcrumb')
<label id="labelRectangle">Noticias</label>
@endsection @section('content')



<body>
    <!-- <div style="margin-bottom:23px"><label id="labelRectangle">Noticias</label></div> -->


    <div class="row">
        <div class="col-md-2 alineado">
            <label class="labelTexto" for="comboboxCategoria">Buscar por categoría</label>
        </div>

        <div class="col-md-3">
            <select style="background-color: #ffffff !important" name="comboboxCategoria" id="comboboxCategoria" class="form-control combobox">
                <option value="">Todas</option>
                @if($categoriaSeleccionada!=null)
                     <option value=" {{ $categoriaSeleccionada->idcatnoticia }}" selected>{{ $categoriaSeleccionada->titulo }}</option>
                     @foreach($categorias as $categoria)
                     @if($categoria->idcatnoticia != $categoriaSeleccionada->idcatnoticia)
                     <option value="{{$categoria->idcatnoticia}}">{{$categoria->titulo}}</option>
                     @endif
                     @endforeach

                @else
                @foreach($categorias as $categoria)
                <option value="{{$categoria->idcatnoticia}}">{{$categoria->titulo}}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-7">
            <div class="box-right">
                <a href="newNoticia" type="button" class="buttonNew-Edit">Nuevo</a>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-2 alineado">
            <label class="labelTexto" for="textBoxLupa">Buscar por Titulo</label>


        </div>
        <div class="col-md-3 ">
            <input style="background-color: #ffffff !important;" type="text" name="busquedaTitulo" id="busquedaTitulo" class="form-control textBoxLupa"
            />
        </div>
    </div>
</body>





<table class="table" style="margin-bottom:16px">
    <thead class="theadPurple">


        <tr>
            <th class scope="col"></th>
            <th class="thWhite" scope="col">Estado</th>
            <th class="thWhite" scope="col">Nombre</th>
            <th class="thWhite" scope="col">Fecha</th>
            <th class="thWhite" scope="col">Contenido</th>
            <th class="thWhite" scope="col">Categoria</th>
        </tr>
    </thead>
    <tbody class="tbody">

        @foreach($noticias as $noticia)
        <tr id="{{$noticia->idnoticia}}">
            <td class="td">

                <button type="button" class="buttonVerMas" data-toggle="modal" data-id="{{ $noticia->idnoticia }}" data-target="#myModal">
                    Ver Mas</button>


            </td>


            <td class="td"> @if($noticia->state ==0)
                <img src="https://res.cloudinary.com/carloandreaguilar/image/upload/v1533329077/MAP%20SALUD%20V3/aprobado.png">               
                 @elseif($noticia->state==1)
                <img src="https://res.cloudinary.com/carloandreaguilar/image/upload/v1533329070/MAP%20SALUD%20V3/denegado.png">                @endif

            </td>
            <td class="td"> {{ $noticia->titulo }} </td>
            <td class="td"> {{ $noticia->created_at }} </td>
            <td class="td"> {{ $noticia->contenido }} </td>
            <td class="td"> {{ $noticia->getCategoryName() }} </td>
        </tr>

        @endforeach

    </tbody>
</table>
{{ $noticias->links() }}
<!-- <div align="right" class="col-md-12" style="padding: 16px 0px 0px 0px">
    <a class="arrowPage"><</a>

    @for($i=0.0;$i<(count($noticias)/10.0);$i++) <button class="rectanglePage">{{$i+1}}</button>
        @endfor
    <a class="arrowPage">></a>
</div> -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modalCustom" role="document">
        <div class="modal-content modalContentCustom">
            <div class="modal-header">
                <div class="row rowModal">
                    <div class="col-md-9">
                        <span id="spanCategorias"></span>

                    </div>
                    <div class="col-md-3 box-right">
                        <button id="btnEditar" href="#" class="buttonNew-Edit">Editar</button>
                    </div>
                </div>
                <div class="row">
                    {{ csrf_field() }}
                    <div class="col-md-12 box-right">
                        <button type="submit" class="buttonDelete" data-dismiss="modal">Eliminar</button>
                    </div>


                </div>

            </div>
            <div class="modal-body">
                <h4 class="modal-title" id="ModalTitle">
                </h4>
                <img style="max-width: 320px" id="imagen" src="">

                <div id="parrafo1" style="padding: 15px 0px"></div>

            </div>
            <div class="modal-footer">


            </div>
        </div>
    </div>
</div>
<a type ="hidden" href="#" id= "buttonBuscar"></a>

<script>
    
    /* jsonnoticias=  jsonnoticias.replace(/&quot;/g,'"');
    console.log(jsonnoticias); */
    var idnoticia
    $(".buttonVerMas").click(function() {
        idnoticia = $(this).data('id');


        $.get('detalleNoticia', {
            idnoticia: idnoticia
        }, function(data) {
            var obj = JSON.parse(data);
            $('#parrafo1').html(obj.noticia.contenido);
            $('#imagen').attr('src', "http://localhost:8000/imagesNot/" + obj.noticia.idnoticia +
                "/" +
                obj.noticia.imagen);
            if (obj.noticia.state == 0) {
                $('#ModalTitle').html(
                    '<img src="https://res.cloudinary.com/carloandreaguilar/image/upload/v1533329077/MAP%20SALUD%20V3/aprobado.png">' +
                    "  " + obj.noticia.titulo);
            } else if (obj.noticia.state == 1) {
                $('#ModalTitle').html(
                    '<img src="https://res.cloudinary.com/carloandreaguilar/image/upload/v1533329077/MAP%20SALUD%20V3/denegado.png">' +
                    obj.noticia.titulo);
            };

            $('#idnoticia').attr('value', obj.noticia.idnoticia);

            $('#spanCategorias').html(obj.nombreCategoriaAbuelo + obj.nombreCategoriaPadre +
                obj.nombreCategoria);


        });

    });
    
    $('#comboboxCategoria').on('change',function() {
        console.log('holaaa');
        var query = $(this).val();
        var link = '{{ route('buscar_por_categoria') }}?query='+query;
        $('#buttonBuscar').attr('href',link);
       document.getElementById('buttonBuscar').click();
        
    });
    $('#btnEditar').click(function() {
        window.location.href = "/editar/" + idnoticia;
    });

    $('#busquedaTitulo').keyup(function(event) {
    if (event.which == 13) {
        
        var query = $(this).val();
        var link = '{{ route('buscar_noticias') }}?query='+query;
        $('#buttonBuscar').attr('href',link);
        document.getElementById('buttonBuscar').click();
    }
     

    });

    $('.buttonDelete').click(function() {

        $.ajax({
            type: "DELETE",
            url: '/eliminar',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                idnoticia: idnoticia
            },
            success: function(data) {
                console.log(data);
            }
        });
        var str = "#" + idnoticia;
        $(str).remove();
    });
    /* $(document).on('change', '#comboboxCategoria', function() {
        $categorias = $noticias;
        

    }); */
</script>


@endsection