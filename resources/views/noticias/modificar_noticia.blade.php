@extends('layouts.app')

<head>
<link href="{{ asset('/css/admin-styles/referencias.css') }}" rel="stylesheet">


 
</head>
@section('breadcrumb')
<label id="labelRectangle">Noticias</label>
@endsection @section('content')



<form role="form" method="POST" action="{{ route('edit_noticial') }}" enctype="multipart/form-data">
<div class="row">
        <div class="col-md-5 alineado">
        <label id="titulo1">Editar Noticia </label>
        </div>
        
        <div class="col-md-7">
            <div class="box-right">
                <button type="submit" style="margin-top:15px"class="buttonNew-Edit">Editar</a>
            </div>
        </div>
    </div>
    
    {{ csrf_field() }}
    <div class="form-group">
    <output id="list">
    <img class="thumb" width="320" src="{{'http://localhost:8000/imagesNot/'.$noticia->idnoticia.'/'.$noticia->imagen}}">
    </output>
              <input class="inputFile" type="file" id="files" name="imagen" value="{{ $noticia->imagen}}" placeholder="Another input">
              <div id="divCambiarImagen" class="buttonCambiarImagen">CAMBIAR IMAGEN</div>
        
        @if($errors->has('imagen'))
          <span style ="color:red;"> La imagen es requerida </span>
        @endif
    </div>
    
    <div class="form-group">
    <label for="inputState" class="label-Roboto">Categoría</label>
            
            <select style="background-color: #ffffff !important" id="inputState" class="form-control combobox" name='idcatnoticia'>
            <option value=" {{ $noticia->getCategoryId() }}" selected>{{ $noticia->getCategoryName()}}</option>
            @forelse ($categorias as $categoria)
            @if($noticia->getCategoryName()!= $categoria->titulo)
              <option value="{{ $categoria->idcatnoticia }}"> {{ $categoria->titulo }}</option>
            @endif
              @empty
    
            @endforelse
          </select>
          </div>
        <input type="hidden"  name="idnoticia" value="{{ $noticia->idnoticia}}">
        <input type="hidden"  name="state" value="{{ $noticia->state}}">
    <div class="form-group">
    <label for="titulo" class="label-Roboto">Titulo</label>
    <input type="text" style= "max-width:550px"class="form-control textBox" name="titulo" value="{{ $noticia->titulo}}">
      @if($errors->has('titulo'))
          <span style ="color:red;"> El titulo es requerido </span>
      @endif
    </div>
    <div class="form-group">
    <label for="contenidol" class="label-Roboto">Contenido</label>
    <textarea id="summary-ckeditor" placeholder="Balabala"  name="contenido" value="">{{ $noticia->contenido}}</textarea>
    @if($errors->has('contenido'))
      <span style ="color:red;"> El contenido es requerido </span>
    @endif
  </div>
     
    
  </form>

  <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
function archivo(evt) {
        var files = evt.target.files; // FileList object
         
          //Obtenemos la imagen del campo "file". 
        for (var i = 0, f; f = files[i]; i++) {         
             //Solo admitimos imágenes.
             if (!f.type.match('image.*')) {
                  continue;
             }
         
             var reader = new FileReader();
             
             reader.onload = (function(theFile) {
                 return function(e) {
                 // Creamos la imagen.
                        document.getElementById("list").innerHTML = ['<img class="thumb" width="300" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                 };
             })(f);
   
             reader.readAsDataURL(f);
         }
  }
               
        document.getElementById('files').addEventListener('change', archivo, false);
        </script>
<script>

   CKEDITOR.replace( 'summary-ckeditor',
{

width: '550px',
height: '189px',
removePlugins: 'elementspath',

removeButtons:'Paste,About,PasteText,PasteFromWord,Undo,Redo,Link,Unlink,Subscript,Superscript',


} );

</script>

@endsection